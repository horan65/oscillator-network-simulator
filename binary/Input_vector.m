clear all

weight=-0.001;
%weight2=-0.001;
a=[0 1 1 1 1 0 1 0 1 0 0 0 1 1 0 1];
a=[1 0 0 0 0 1 0 1 0 1 1 1 0 0 1 0]
pieces=size(a,2);
w=zeros(size(a));
w(2)=weight;
%w(pieces)=weight2;
w=toeplitz(w);
M=Spin_torque_array(a,w);
len=size(M,1);
dif=zeros(size(a));
for i=2:pieces
   dif(i-1)=M(len,i).* M(len,i-1+pieces) - M(len,i+pieces).* M(len,i-1);
   mx=max(M(round(0.8*len):len,i));
   dif(i-1)=dif(i-1)/(mx*mx);
end