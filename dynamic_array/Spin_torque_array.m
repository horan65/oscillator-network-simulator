function M=Spin_torque_array()


options = odeset('RelTol',1.0E-5,'AbsTol', 1.0E-6, 'MaxStep', 1.0E-10);

global Params
%The simulation time. Oscillations happen in the GHz range, so simulation
%time should be in the nanosecond range. 
Params.Max_time = 20.0E-8;


%This is material parameter for permalloy (a widely used magnetic material)
%Leave this number unchanged
Params.Ms = 8.6E+5;

%Further physical parameters. Leave them unchanged
%These parameters are describing the geometry of the magnetic layer
%this is a cylindrical geometry:
Params.c = 2.210E+5;
Params.alfa =  0.008;  
Params.N = [0.2 0.2 0.6];
%this aprameter describes the polarization of the current
Params.S = [0.0 0.0 1];


%the number of the oscillators in the array
Params.Size=9;

%the input current of the oscillators
Params.A = ones(Params.Size,100)*100;

init=100;
max=1000;
for i=1:60
        Params.A(1:9,i)=init;
        
        Params.A(5,i)=init;
end

step=100;
prev=init;
for i=61:100
        Params.A(1:9,i)=init;
        
        Params.A(5,i)=prev+step;
        if Params.A(5,i)>max
           Params.A(5,i)=max;
        end
        prev=Params.A(5,i);
end


%generation of the initial conditions
Phi = 0.2;
Theta = 0.8;
Mxinit = ones(Params.Size,1)*cos(Phi)*sin(Theta);
Myinit = ones(Params.Size,1)*sin(Phi)*sin(Theta);
Mzinit = ones(Params.Size,1)*cos(Theta);
%I generate the initial conditions randomly
for i=1:Params.Size
    Phi = rand*(pi/2);
    Theta = rand*(pi/2);
    Mxinit(i)=cos(Phi)*sin(Theta);
    Myinit(i)=sin(Phi)*sin(Theta);
    Mzinit(i)=cos(Theta);
end


Weight=[0 0.0001 0 0 0 0 0 0 0 ;
        0.0001 0 0.0001 0 0 0 0 0 0;
        0 0.0001 0 0.0001 0 0 0 0 0 ;
        0 0  0.0001 0 0.0001 0 0 0 0 ;
        0 0 0 0.0001 0 0.0001 0 0 0;
        0 0 0 0    0.0001 0 0.0001 0 0 ;
        0 0 0 0 0     0.0001 0 0.0001 0 ;
         0 0 0 0 0 0      0.0001 0 0.0001;
        0 0 0 0 0 0 0       0.0001 0 ] ;
%the weights matrices, that are describing the strength of the coupling
Params.Cx =Weight;
Params.Cy =Weight;
Params.Cz =Weight;


%Solvin the equations
[t,M] = ode45('diff_spintorque_array',[0 Params.Max_time],[Mxinit Myinit Mzinit], options);
%after this line the result is variable 'M'
%when the number of oscillators is 'N' and the time of the simulation is
%'t', than M is a matrix with size (t x 3N)
%the first N parts are containing the first components of the oscillators
%and so on...





