function dM = diff_spintorque_array(t,M)

global Params

Size=Params.Size;
M=[M(1:Size) M(Size+1:2*Size) M(2*Size+1:3*Size)];




dM = zeros(3*Size,1);

InPerc=round( t/(Params.Max_time/100));
if InPerc==0
    InPerc=1;
end
delta=(Params.N(3)-Params.N(2));
eta=Params.Ms*Params.c;
alpha=Params.alfa;
c=Params.c;
A=Params.A(:,InPerc);
Cx = Params.Cx;
Cy = Params.Cy;
Cz = Params.Cz;

%coupling of x:
CoupX=eta* ( (Cz * M(:,3)) .* M(:,2) -  (Cy * M(:,2)) .* M(:,3) )- eta*alpha * ( (Cy*M(:,2)) .*M(:,1) .*M(:,2)  - (Cx*M(:,1)) .*M(:,2) .*M(:,2) - (Cx*M(:,1)) .*M(:,3) .*M(:,3)  + (Cz*M(:,3)) .*M(:,1) .*M(:,3));

%coupling of y:
CoupY=eta* ( (Cx * M(:,1)) .* M(:,3) -  (Cz * M(:,3)) .* M(:,1) )- eta*alpha * ( (Cz*M(:,3)) .*M(:,2) .*M(:,3)  - (Cy*M(:,2)) .*M(:,3) .*M(:,3) - (Cy*M(:,2)) .*M(:,1) .*M(:,1)  + (Cx*M(:,1)) .*M(:,2) .*M(:,1));

%coupling of z:
CoupZ=eta* ( (Cy * M(:,2)) .* M(:,1) -  (Cx * M(:,1)) .* M(:,2) )- eta*alpha * ( (Cx*M(:,1)) .*M(:,3) .*M(:,1)  - (Cz*M(:,3)) .*M(:,1) .*M(:,1) - (Cz*M(:,3)) .*M(:,2) .*M(:,2)  + (Cy*M(:,2)) .*M(:,3) .*M(:,2));

dM(1:Size)=-eta*delta*M(:,2).*M(:,3)+alpha*eta*delta*M(:,1).*M(:,3).*M(:,3)-c*A.*M(:,1).*M(:,3)+CoupX;
dM(Size+1:2*Size)=eta*delta*M(:,1).*M(:,3)+alpha*eta*delta*M(:,2).*M(:,3).*M(:,3)-c*A.*M(:,2).*M(:,3)+CoupY;
dM(2*Size+1:3*Size)=-alpha*eta*delta*M(:,3).*(M(:,1).*M(:,1)+M(:,2).*M(:,2))+c*A.*(M(:,1).*M(:,1)+M(:,2).*M(:,2))+CoupZ;
end

