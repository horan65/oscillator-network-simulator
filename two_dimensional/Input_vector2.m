clear all


img=imread('1.bmp')+60;
weight=0.001;


a=reshape(img,1,256);
pieces=256;

w=zeros(size(a));
for i=1:16
        c=(i-1)*16+1;
        if c+1>256
            w(c-256+1)=weight;
        else
        w(c+1)=weight;
        end
        
        if c-1>0
            w(c-1)=weight;
        else
            w(16*16)=weight;
        end
        
        if c+16>256
           w(c-256+26)=weight; 
        else
        w(c+16)=weight;
        end
        
        if c-16>0
            w(c-16)=weight;
        else
            w(16*16-c)=weight;    
        end
end


M=Spin_torque_array(a,w);
len=size(M,1);
dif=zeros(size(a));
for i=2:pieces
   dif(i-1)=M(len,i).* M(len,2+pieces) - M(len,i+pieces).* M(len,2);
   %dif(i-1)=M(len,i).* M(len,i-1+pieces) - M(len,i+pieces).* M(len,i-1);
   mx=max(M(round(0.8*len):len,i));
   dif(i-1)=dif(i-1)/(mx*mx);
end
img=reshape(dif,16,16);