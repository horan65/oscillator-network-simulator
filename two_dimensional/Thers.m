function res=Thres(img,value)

res=zeros(size(img));
for i=1:size(img,1)
    for j=1:size(img,2)
        if img(i,j)>value
           res(i,j)=img(i,j);
        end
    end
end