clear all;
OscNumX=5;
OscNumY=3;
WeightMat=zeros(OscNumX*OscNumY);
wl=0.001;
wr=0.001;
wu=0.001;
wd=0.001;
for i=1:(OscNumX*OscNumY)
    for j=1:(OscNumX*OscNumY)
        %horizontal neighbours
        if mod(i,OscNumX)==1
           WeightMat(i,i+OscNumX-1)=wl;
        elseif j==i-1
           WeightMat(i,j)=wl; 
        end
        
        if mod(i,OscNumX)==0
           WeightMat(i,i-OscNumX+1)=wr; 
        elseif j==i+1
           WeightMat(i,j)=wr;
        end
        %vertical neighbours
        if i<=OscNumX
            WeightMat(i,OscNumX*(OscNumY-1)+i)=wu;
        elseif j==i-OscNumX
            WeightMat(i,j)=wu;
        end
        
        if i>(OscNumX*(OscNumY-1))
            WeightMat(i,i-OscNumX*(OscNumY-1))=wd;
        elseif j==i+OscNumX
            WeightMat(i,j)=wd;
        end
    end
end