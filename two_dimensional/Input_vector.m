clear all


img=uint16(imread('2.bmp'))/2;


OscNumX=size(img,1);
OscNumY=size(img,2);

a=reshape(img,1,OscNumX*OscNumY)+60;
pieces=OscNumX*OscNumY;

WeightMat=zeros(OscNumX*OscNumY,OscNumX*OscNumY);
%WeightMat=zeros(size(a));
wl=0;
wln=0;
wr=0.01;
wrn=0;
wu=0.01;
wun=0;
wd=0.01;
wdn=0;
for i=1:(OscNumX*OscNumY)
    for j=1:(OscNumX*OscNumY)
        %horizontal neighbours
        if mod(i,OscNumX)==1
           WeightMat(i,i+OscNumX-1)=wln;
        elseif j==i-1
           WeightMat(i,j)=wl; 
        end
        
        if mod(i,OscNumX)==0
           WeightMat(i,i-OscNumX+1)=wrn; 
        elseif j==i+1
           WeightMat(i,j)=wr;
        end
        %vertical neighbours
        if i<=OscNumX
            WeightMat(i,OscNumX*(OscNumY-1)+i)=wun;
        elseif j==i-OscNumX
            WeightMat(i,j)=wu;
        end
        
        if i>(OscNumX*(OscNumY-1))
            WeightMat(i,i-OscNumX*(OscNumY-1))=wdn;
        elseif j==i+OscNumX
            WeightMat(i,j)=wd;
        end
    end
end


M=Spin_torque_array(a,WeightMat);
len=size(M,1);
dif=zeros(size(a));
dif(1)=0;
for i=2:pieces
   dif(i)=M(len,i).* M(len,1+pieces) - M(len,i+pieces).* M(len,1);
   %dif(i-1)=M(len,i).* M(len,i-1+pieces) - M(len,i+pieces).* M(len,i-1);
   mx=max(M(round(0.8*len):len,i));
   dif(i-1)=dif(i-1)/(mx*mx);
end
img=reshape(dif,OscNumX,OscNumY);
img=abs(img);
norm=60/max(max(img));
img=img*norm;
image(img);
